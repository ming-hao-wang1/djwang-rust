use sqlx::{MySql, Pool};
use std::time::Duration;
use sqlx::mysql::{MySqlPoolOptions, MySqlConnectOptions};
use crate::config::Config;

pub async fn connect_db(config: &Config) -> Result<Pool<MySql>, sqlx::Error> {
    let connect_options = MySqlConnectOptions::new()
        .host(&config.database_host) // usually localhost
        .port(config.database_port) // usually 3306
        .database(&config.database_name)
        .username(&config.database_user)
        .password(&config.database_password);

    MySqlPoolOptions::new()
        .max_connections(5)
        .max_lifetime(Duration::from_secs(3600)) // 设置连接的最大生命周期
        .connect_with(connect_options)
        .await
}
