use sqlx::FromRow;
use serde::Serialize;

#[derive(Serialize, FromRow, Debug)]
pub struct Domain {
    pub id: i32,
    pub name: String,
    pub email: String,
}
