use actix_web::{App, HttpServer};
use actix_web::web::Data;
use dotenv::dotenv;
use crate::config::Config;
use crate::db::connect_db;
use crate::routes::config_routes;
// 引入相关的库
use env_logger;
use log::info;

mod config;
mod db;
mod models;
mod handlers;
mod routes;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // 初始化日志
    // std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_LOG", "info,actix_web=debug,actix_server=info");
    env_logger::init();
    dotenv().ok();


    let config = Config::from_env();
    let pool = match connect_db(&config).await {
        Ok(pool) => pool,
        Err(e) => {
            eprintln!("Error while establishing the connection to the database: {}", e);
            std::process::exit(1); // 如果连接失败，退出程序
        }
    };
    let server_address = format!("10.0.5.205:{}", config.server_port);
    info!("server address:{}",server_address);
    HttpServer::new(move || {
        App::new()
            .app_data(Data::new(pool.clone()))  // 使用Data::new()来包装pool
            .configure(config_routes)
    })
        .bind(server_address)?
        .run()
        .await
}
