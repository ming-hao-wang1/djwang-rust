// 在routes.rs
use actix_web::{HttpResponse, Responder, web};
use crate::handlers;
pub fn config_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/users/{id}")
            .route(web::get().to(handlers::get_user_by_id))
    );
    cfg.service(
        web::resource("/")
            .route(web::get().to(health_check))
    );
}



async fn health_check() -> impl Responder {
    HttpResponse::Ok().body("Server is up and running!")
}
