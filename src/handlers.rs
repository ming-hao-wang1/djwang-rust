use actix_web::{web, Responder, HttpResponse};
use log::{error, info};
use sqlx::MySqlPool;
use crate::models::Domain;


pub async fn get_user_by_id(
    pool: web::Data<MySqlPool>,
    user_id: web::Path<i32>,
) -> impl Responder {
    // 获取 user_id 的值
    let user_id_value = *user_id;

    // 查询数据库
    let result = sqlx::query_as::<_, Domain>(
        "SELECT * FROM domain WHERE id = ?",
    )
        .bind(user_id_value)
        .fetch_one(pool.get_ref())
        .await;

    match result {
        Ok(user) => {
            info!("Fetched user: {:?}", user);
            println!("data:{}", user.email);
            HttpResponse::Ok().json(user)
        },
        Err(e) => {
            if let sqlx::Error::RowNotFound = e {
                info!("User with id: {} not found", user_id_value);
                println!("e:{}",e.to_string());
                HttpResponse::NotFound().json("User not found")
            } else {
                error!("Error fetching user: {}", e);
                println!("e:{}",e.to_string());

                HttpResponse::InternalServerError().finish()
            }
        },
    }
}
