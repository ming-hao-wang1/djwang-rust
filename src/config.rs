use dotenv::dotenv;

pub struct Config {
    pub database_host: String,
    pub database_port: u16,
    pub database_name: String,
    pub database_user: String,
    pub database_password: String,
    pub server_port: String,
}

impl Config {
    pub fn from_env() -> Self {
        dotenv().ok();
        dotenv::from_filename("./application.env").unwrap();
        let database_url = dotenv::var("DATABASE_URL").expect("DATABASE_URL must be set");
        let database_user = dotenv::var("DB_USERNAME").expect("DATABASE_URL must be set");
        let database_password = dotenv::var("DB_PASSWORD").expect("DATABASE_URL must be set");

        // 解析连接字符串
        let url = url::Url::parse(&database_url).expect("Invalid DATABASE_URL");

        // 从连接字符串中获取各个参数
        let database_host = url.host_str().expect("Missing host in DATABASE_URL").to_string();
        let database_port = url.port().unwrap_or(3306); // 默认使用 3306 端口
        let database_name = url.path_segments().expect("Missing path in DATABASE_URL").next().expect("No path segment in DATABASE_URL").to_string();

        Self {
            database_host,
            database_port,
            database_name,
            database_user,
            database_password,
            server_port: dotenv::var("SERVER_PORT").expect("SERVER_PORT must be set"),
        }
    }
}
